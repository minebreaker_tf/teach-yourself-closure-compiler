// import * as React from "react"
// import * as ReactDOM from "react-dom"
const React = require("react")
const ReactDOM = require("react-dom")
const ImMap = require("immutable/dist/immutable").Map
const DateTime = require("luxon/build/node/luxon").DateTime

class Application extends React.Component {

    constructor(props) {
        super(props);
        this.state = { date: DateTime.local().toISO() }
    }

    componentDidMount() {
        this.timer = setInterval(() => this.setState({ date: DateTime.local().toISO() }), 500)
    }

    componentWillUnmount() {
        clearTimeout(this.timer)
    }

    render() {
        return (
            <div>
                <h1>hello, closure compiler!</h1>
                <p>User: {this.props.user.get("firstName")} {this.props.user.get("lastName")}</p>
                <p>Current date: {this.state.date}</p>
            </div>
        )
    }
}

ReactDOM.render(
    <Application user={ImMap({ firstName: "John", lastName: "Smith" })} />,
    document.getElementById("app")
)
