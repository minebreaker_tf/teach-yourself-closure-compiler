const path = require("path")
const ClosurePlugin = require("closure-webpack-plugin")

module.exports = {
    entry: "./out/closure/index.js",
    output: {
        path: path.resolve(__dirname, "out/webpack"),
        filename: "index.js"
    },
    mode: "development",
    devtool: "inline-source-map",
    module: {
        rules: [
            {
                test: /\.js(x)?/,
                use: [
                    "babel-loader"
                ]
            }
        ]
    },
    plugins: [
        // new ClosurePlugin({ mode: "STANDARD" })
    ],
    resolve: {
        extensions: [".js", ".jsx"]
    }
}
